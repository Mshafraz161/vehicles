<!DOCTYPE HTML>
<html>
        <head>
            <title>Pixametic Car Sales Company</title>
            <link href="<?php echo (base_url()); ?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
            
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>   
            <!--slider-->
            <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/script.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>assets/js/superfish.js"></script>
            <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet"> 
            <style>
                @import url('https://fonts.googleapis.com/css?family=Lobster');
                h4{
                    font-family: 'Lobster', cursive;
                    font-size: 30px;
                    color: #D54F30;
                }
                

            </style> 
        </head>
    <body onload="startTime()">
        <div class="header-bg">
            <div class="wrap"> 
                <div class="h-bg">
                    <div class="total">
                        <div class="header">
                            <div class="box_header_user_menu">
                                <ul class="user_menu">

                                    <li class=""><a href="<?php echo (base_url());?>Users/registration"><div class="button-t"><span>Create an Account</span></div></a></li>
                                    <li class="last"><a href="<?php echo (base_url());?>Users/login"><div class="button-t"><span>Log in</span></div></a></li></ul>
                            </div>

                            <div class="header-right">
                                <ul class="follow_icon">
                                    <li><a href="<?php echo (base_url()); ?>DE/language"><img src="<?php echo base_url(); ?>assets/images/icon.png" alt=""/></a></li>
                                    <li><a href="<?php echo (base_url()); ?>DU/language_dutch"><img src="<?php echo base_url(); ?>assets/images/icon1.png" alt=""/></a></li>
                                    
                                </ul>
                            </div>

                            <div class="header-bot">
                                <h4> PIXAMETIC <span style="font-size: 20px; color: black;">Vehicle</span> </h4> 
                                
                                <div class="clear"></div> 
                            </div>		
                        </div>	
                        <div class="menu"> 	
                            <div class="top-nav"> 
                                <ul>
                                    <li><a href="<?php echo base_url(); ?>Vehicles/index">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>Vehicles/about">About</a></li>
                                    <li><a href="<?php echo base_url(); ?>Vehicles/single">For Sale</a></li>
                                    <li><a href="<?php echo base_url(); ?>Vehicles/specials">specials</a></li>
                                    <li><a href="<?php echo base_url(); ?>Vehicles/contact">Contact</a></li>
                                </ul>
                                <div class="clear"></div> 
                            </div>
                        </div>
                        <div class="banner-top">
                            <div class="header-bottom">
                                <div class="header_bottom_right_images">
                                    <div id="slideshow">
                                        <ul class="slides">
                                            <li><canvas ></canvas><img src="<?php echo base_url(); ?>assets/images/banner4.jpg" alt="Marsa Alam underawter close up" ></li>
                                            <li><canvas></canvas><img src="<?php echo base_url(); ?>assets/images/banner2.jpg" alt="Turrimetta Beach - Dawn" ></li>
                                            <li><canvas></canvas><img src="<?php echo base_url(); ?>assets/images/banner3.jpg" alt="Power Station" ></li>
                                            <li><canvas></canvas><img src="<?php echo base_url(); ?>assets/images/banner1.jpg" alt="Colors of Nature" ></li>
                                        </ul>

                                        <span class="arrow previous"></span>
                                        <span class="arrow next"></span>
                                    </div>
                                </div>
                                </div>


                                <div style="margin-left: 840px;">
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=260432411060655";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page" data-href="https://www.facebook.com/Pixametic" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Pixametic" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Pixametic">Pixametic</a></blockquote></div>
</div>
    <div class="container">
    <h1 style="margin-right: 20px">Browse Your Vehicle Here</h1>
</center>
    
    <br />
    <button class="btn btn-success" onclick="add_book()" style="background-color: rgb(255, 144, 0); width: 500px; height: 65px; margin-left: 150px"><i class=""></i> Browse Vehicle</button>
    
    <br />
    <br />
   


  <script type="text/javascript">
  
    function add_book()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

   
function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo base_url('browse/browse_add')?>";
      }
      else
      {
        url = "<?php echo base_url('browse/browse_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    

  </script>
  
  

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Quick Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="browse_id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">name</label>
              <div class="col-md-9">
                <input name="name" placeholder="name" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">vehicle name</label>
              <div class="col-md-9">
                <input name="vname" placeholder="vehicle name" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">model number</label>
              <div class="col-md-9">
                <input name="mnumber" placeholder="model number" class="form-control" type="text">

              </div>
            </div>
             <div class="form-group">
              <label class="control-label col-md-3">contact</label>
              <div class="col-md-9">
                <input name="cnumber" placeholder="contact" class="form-control" type="text">
              </div>
            </div>
            

          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
                     
                                

<div class="clear" style="margin-left: 480px; margin-top: 112px">
<script type="text/javascript">
function startTime() {
    var today = new Date();
    var hr = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();
    ap = (hr < 12) ? "<span>AM</span>" : "<span>PM</span>";
    hr = (hr == 0) ? 12 : hr;
    hr = (hr > 12) ? hr - 12 : hr;
    //Add a zero in front of numbers<10
    hr = checkTime(hr);
    min = checkTime(min);
    sec = checkTime(sec);
    document.getElementById("clock").innerHTML = hr + ":" + min + ":" + sec + " " + ap;
    
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    var curWeekDay = days[today.getDay()];
    var curDay = today.getDate();
    var curMonth = months[today.getMonth()];
    var curYear = today.getFullYear();
    var date = curWeekDay+", "+curDay+" "+curMonth+" "+curYear;
    document.getElementById("date").innerHTML = date;
    
    var time = setTimeout(function(){ startTime() }, 500);
}
function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
</script>
<div id="clockdate" style="width: 400px; height: 150px">
  <div class="clockdate-wrapper">
    <div id="clock"></div>
    <div id="date"></div>
  </div>
</div>
</div>








