<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Learn PHP CodeIgniter Framework with AJAX and Bootstrap</title>
    <link href="<?php echo (base_url()); ?>assets/bootstsrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo (base_url()); ?>assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
   
  </head>
  <body>


  <div class="container">
    <h1>Learn PHP CodeIgniter Framework with AJAX and Bootstrap</h1>
</center>
    <h3>Book Store</h3>
    <br />
    <button class="btn btn-success" onclick="add_book()"><i class="glyphicon glyphicon-plus"></i> Add Book</button>
    <br />
    <br />
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
					<th>Quick ID</th>
					<th>Quick Number</th>
					<th>Quick Title</th>
					<th>By Whom</th>
					<th>Subject</th>

          <th style="width:125px;">Action
          </p></th>
        </tr>
      </thead>
      <tbody>
				<?php foreach($books as $quick){?>
				     <tr>
				         <td><?php echo $quick->quick_id;?></td>
				         <td><?php echo $quick->quick_number;?></td>
								 <td><?php echo $quick->quick_title;?></td>
								<td><?php echo $quick->by_whom;?></td>
								<td><?php echo $quick->subject;?></td>
								<td>
									<button class="btn btn-warning" onclick="edit_book(<?php echo $quick->quick_id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
									<button class="btn btn-danger" onclick="delete_book(<?php echo $quick->quick_id;?>)"><i class="glyphicon glyphicon-remove"></i></button>


								</td>
				      </tr>
				     <?php }?>



      </tbody>

      <tfoot>
        <tr>
          <th>Quick ID</th>
          <th>Quick Number</th>
          <th>Quick Title</th>
          <th>By Whom</th>
          <th>Subject</th>
          <th>Action</th>
        </tr>
      </tfoot>
    </table>

  </div>

  <script src="<?php echo (base_url());?>assets/js/jquery-3.1.1.min.js"></script>
  <script src="<?php echo (base_url());?>assets/bootstsrap/js/bootstrap.min.js"></script>
  <script src="<?php echo (base_url());?>assets/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo (base_url());?>assets/datatables/js/dataTables.bootstrap.js"></script>


  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;


    function add_book()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_book(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('quick/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="quick_id"]').val(data.quick_id);
            $('[name="quick_number"]').val(data.quick_number);
            $('[name="quick_title"]').val(data.quick_title);
            $('[name="by_whom"]').val(data.by_whom);
            $('[name="subject"]').val(data.subject);


            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Book'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo base_url('quick/quick_add')?>";
      }
      else
      {
        url = "<?php echo base_url('quick/quick_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_book(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo base_url('quick/quick_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }

  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Quick Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="book_id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">quick number</label>
              <div class="col-md-9">
                <input name="quick_number" placeholder="quick_number" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Quick Title</label>
              <div class="col-md-9">
                <input name="quick_title" placeholder="Quick_title" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">By Whom</label>
              <div class="col-md-9">
								<input name="by_whom" placeholder="by whom" class="form-control" type="text">

              </div>
            </div>
						<div class="form-group">
							<label class="control-label col-md-3">Subject</label>
							<div class="col-md-9">
								<input name="subject" placeholder="subject" class="form-control" type="text">

							</div>
						</div>

          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  </body>
</html>
