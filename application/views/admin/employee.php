
<!DOCTYPE HTML>
<html>
<head>
<title>Admin Dashboard</title>
<link href="<?php echo (base_url()); ?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo (base_url()); ?>assets/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" media="all" />
    
    
<script type="text/javascript" src="<?php echo (base_url()); ?>assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo (base_url()); ?>assets/js/jquery"></script>
<script type="text/javascript" src="<?php echo (base_url()); ?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo (base_url()); ?>assets/js/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>   
    
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'> 
<style>
@import url('https://fonts.googleapis.com/css?family=Lobster');
h4{
	font-family: 'Lobster', cursive;
	font-size: 30px;
	color: #D54F30;
}
</style>   
</head>
<body>


<div class="banner-top">
			<div class="header-bottom">
				 
		    <div class="section group">
				<div class="col span_2_of_c">
				  <div class="contact-form">
				  	<h3 style="margin-left: 500px">Welcome Admin</h3>

                    <a href="Admin_index"><button type="button" class="btn btn-warning" style= "width:200px; height:80px; margin-bottom:60px">BackTo Admin Home</button></a>

				  	<div class="header_bottom_right_images">
				 	<div class="about_wrapper"><h3>Managing Employee Here</h3>
					</div>
					    <form method="post" action="<?php $_PHP_SELF ?>">
					    	<div>
						    	<span><label>EMPLOYEE NAME</label></span>
						    	<span><input name="emp_name"  id="emp_name" type="text" class="textbox" required=""></span>
						    </div>
                            <div>
                                <span><label>DATE-OF-BIRTH</label></span>
                                <span><input name="dob" id="dob" type="text" class="textbox date-picker" date="" data-date-format="yyyy-mm-dd" required=""></span>
                            </div>
						    <div>
						    	<span><label>E-MAIL</label></span>
						    	<span><input name="email" id="email" type="text" class="textbox" required=""></span>
						    </div>
						    <div>
						     	<span><label>MOBILE</label></span>
						    	<span><input name="phone"  id="phone" type="text" class="textbox" required=""></span>
						    </div>
						     
						    <input type="hidden" name="emp_id" id="emp_id" value=""/>
						    <input type="submit" id="btn1" name="submit" value="DeleteEmployee" style="display:none;">
						    <input type="submit" id="btn2" name="submit" value="UpdateEmployee" style="display:none;">
						    <input type="submit" id="btn3"  name="submit" value="Addemployee">
					    </form>
				  </div>
  				</div><div class="clear"></div>
			</div>
		</div>
                <div class="clear"></div>
                <br/><br/>
                <div class="col-sm-12">

                    <table class="table" id="datatable" style="width:100%">
                    
                        <thead style="background:#222;color:white;">
                            <tr style="padding:20px; ">
                            <th style="margin-right: 10px">Employee Name</th>
                            <th style="margin-right: 10px">Phone</th>
                            <th>Email</th>
                            <th>Update</th>
                            <th>Delete</th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            <?php
                                foreach($list as $row){?>
                            <tr>
                            <td><?php echo $row->emp_name;?></td>
                            <td><?php echo $row->email;?></td>
                            <td><?php echo $row->phone;?></td>
                            <td><center><button onclick="updateEmp(this.id);" id="<?php echo $row->emp_id;?>" class="btn btn-info">Update</button></center></td>
                            <td><center><button onclick="deleteEmp(this.id);" id="<?php echo $row->emp_id;?>" class="btn btn-warning">Delete</button></center></td>
                            </tr>
                            
                            <?php } ?>
                        
                        </tbody>
                    </table>
                
                    
                    
    </div>
	</div>
		<div class="clear"></div>

    <style>
    
        th{
            padding: 20px; font-weight: 700;
        }
    </style>
    <script>
    $(document).ready(function() {
        $( "#dob" ).datepicker();
    });
    function setDateFormat(date_format) {
        $( "#dob" ).datepicker( "option", "dateFormat", date_format );
    }
</script>
<script>
    
    window.onload=function(){
        $("#datatable").DataTable();
    }
    
    function updateEmp(id){
        $.post("<?php echo base_url();?>Admin/get_single_employee",
    {
        id: id,
    },
    function(data, status){
            var newval  =   JSON.parse(data);
            document.getElementById("emp_name").value=newval[0]["emp_name"];
            document.getElementById("dob").value=newval[0]["dob"];
            document.getElementById("email").value=newval[0]["email"];
            document.getElementById("phone").value=newval[0]["phone"];
            
            
            document.getElementById("btn1").style.display="none";
            document.getElementById("btn2").style.display="none";
            document.getElementById("btn3").style.display="none";
            document.getElementById("btn2").style.display="block";
    });
    }
    function deleteEmp(id){
        $.post("<?php echo base_url();?>Admin/get_single_employee",
    {
        id: id,
    },
    function(data, status){
            var newval  =   JSON.parse(data);
            document.getElementById("emp_name").value=newval[0]["emp_name"];
            document.getElementById("dob").value=newval[0]["dob"];
            document.getElementById("email").value=newval[0]["email"];
            document.getElementById("phone").value=newval[0]["phone"];
            
            
            document.getElementById("btn1").style.display="none";
            document.getElementById("btn2").style.display="none";
            document.getElementById("btn3").style.display="none";
            document.getElementById("btn1").style.display="block";
    });
    }
    </script>