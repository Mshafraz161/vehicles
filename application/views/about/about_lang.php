<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
</head>
<body>
<ul class="menu-lang">
	<?php
		$this->load->view('layout/header');
	?>

</ul>
<?php
	echo $this->lang->line('heading_about');
	?>
<ul class="nav-menu">
	<li><a href="<?php echo (base_url());?><?php $this->uri->segment(1)?>language"></a><?php echo $this->lang->line('menu_link_about')?></li>
</ul>
<div style="margin-left: 100px">
<?php
	echo $this->lang->line('heading_about');
	echo $this->lang->line('body_text');
?>
</div>

</body>
</html>