<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Quick View </title>
    <link href="<?php echo (base_url()); ?>assets/bootstsrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo (base_url()); ?>assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo (base_url()); ?>assets/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="<?php echo (base_url()); ?>assets/js/bootstrap-datepicker.js"></script>
   
  </head>
  <body>


  <div class="container">
    <h1 style="margin-right: 20px">Welcome Admin Add A Quick View Here</h1>
</center>
    
    <br />
    <button class="btn btn-success" onclick="add_book()" style="background-color: rgb(255, 144, 0);"><i class="glyphicon glyphicon-plus"></i> Quick View</button>
   <a href="<?php echo (base_url());?>Admin/admin_index"><button class="btn btn-success" style="background-color: rgb(255, 144, 0);"><i class=""></i> Back To Admin</button></a> 
    <br />
    <br />
    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Events ID</th>
          <th>Messages</th>
          <th>Place</th>
          <th>Day</th>
          

          <th style="width:125px;">Action
          </p></th>
        </tr>
      </thead>
      <tbody>
       
<?php foreach($event as $events){?>
             <tr>
                 <td><?php echo $events->events_id;?></td>
                 <td><?php echo $events->message;?></td>
                 <td><?php echo $events->place;?></td>
                <td><?php echo $events->day;?></td>
                
                <td>
                  <button class="btn btn-warning" onclick="edit_book(<?php echo $events->events_id;?>)"><i class="glyphicon glyphicon-pencil"></i></button>
                  <button class="btn btn-danger" onclick="delete_book(<?php echo $events->events_id;?>)"><i class="glyphicon glyphicon-remove"></i></button>


                </td>
              </tr>
             <?php }?>


      </tbody>

      <tfoot>
        <tr>
          <th>Events ID</th>
          <th>Messages</th>
          <th>Place</th>
          <th>Day</th>
          
          <th>Action</th>
        </tr>
      </tfoot>
    </table>

  </div>

  <script src="<?php echo (base_url());?>assets/js/jquery-3.1.1.min.js"></script>
  <script src="<?php echo (base_url());?>assets/bootstsrap/js/bootstrap.min.js"></script>
  <script src="<?php echo (base_url());?>assets/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo (base_url());?>assets/datatables/js/dataTables.bootstrap.js"></script>


  <script type="text/javascript">
  $(document).ready( function () {
      $('#table_id').DataTable();
  } );
    var save_method; //for save method string
    var table;


    function add_book()
    {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_book(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals

      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('events_controller/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="events_id"]').val(data.events_id);
            $('[name="message"]').val(data.message);
            $('[name="place"]').val(data.place);
            $('[name="day"]').val(data.day);
           


            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Book'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo base_url('events_controller/events_add')?>";
      }
      else
      {
        url = "<?php echo base_url('events_controller/events_update')?>";
      }

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }

    function delete_book(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data from database
          $.ajax({
            url : "<?php echo base_url('events_controller/events_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               
               location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

      }
    }

  </script>
  <script type="text/javascript">
    <script>
    $(document).ready(function() {
        $( "#day" ).datepicker();
    });
    function setDateFormat(date_format) {
        $( "#day" ).datepicker( "option", "dateFormat", date_format );
    }
</script>
  </script>

  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Quick Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="book_id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Message</label>
              <div class="col-md-9">
                <input name="message" placeholder="message" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">place</label>
              <div class="col-md-9">
                <input name="place" placeholder="place" class="form-control" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">day</label>
              <div class="col-md-9">
                <input name="day" id="day" placeholder="day" class="form-control" type="text">

              </div>
            </div>
            

          </div>
        </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  </body>
</html>
