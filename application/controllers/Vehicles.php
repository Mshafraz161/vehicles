<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicles extends CI_Controller {

	
	public function index()
	{	
		
		$this->load->view('home/index');
		$this->load->view('layout/footer');
	
	}
	public function about()
	{	
		$this->load->view('layout/header');
		$this->load->view('about/about');
		$this->load->view('layout/footer');
	}

	public function contact(){
		$this->load->view('layout/header');
		$this->load->view('contact/contact');
		$this->load->view('layout/footer');
	}

	public function single(){
		$this->load->view('layout/header');
		$this->load->view('single/single');
		$this->load->view('layout/footer');
	}
	public function specials(){
		$this->load->view('layout/header');
		$this->load->view('specials/specials');
		$this->load->view('layout/footer');
	}

}
