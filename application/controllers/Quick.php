<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quick extends CI_Controller {

	

	 public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('quick_model');
	 	}


	public function index()
	{

		$data['books']=$this->quick_model->get_all_quicks();
		$this->load->view('book_view',$data);
	}
	public function quick_add()
		{
			$data = array(
					'quick_number' => $this->input->post('quick_number'),
					'quick_title' => $this->input->post('quick_title'),
					'by_whom' => $this->input->post('by_whom'),
					'subject' => $this->input->post('subject'),
				);
			$insert = $this->quick_model->quick_add($data);
			echo json_encode(array("status" => TRUE));
		}
		public function ajax_edit($id)
		{
			$data = $this->quick_model->get_by_id($id);



			echo json_encode($data);
		}

		public function quick_update()
	{
		$data = array(
				'quick_number' => $this->input->post('quick_number'),
				'quick_title' => $this->input->post('quick_title'),
				'by_whom' => $this->input->post('by_whom'),
				'subject' => $this->input->post('subject'),
			);
		$this->quick_model->quick_update(array('quick_id' => $this->input->post('quick_id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function quick_delete($id)
	{
		$this->quick_model->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}



}
