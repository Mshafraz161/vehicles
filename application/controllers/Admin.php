<?php

class Admin extends CI_Controller {

    public function customers() {
        $this->load->model('AdminDb');
        $this->add_new_customer();
        $data['list'] = $this->AdminDb->getCustomers();
        $this->load->view('admin/admin.php', $data);
        //$this->load->view('layout/footer');
    }

    //function to adding customer to database..
    public function add_new_customer() {

        //button name and value from admin.php(views)
        if ($this->input->post("submit") == "Addme") {

            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $mobile = $this->input->post('mobile');
            $dob = $this->input->post('dob');
            $address = $this->input->post('address');

            $customers = array(); //parameter which i pass in model
            $customers['name'] = $name;
            $customers['email'] = $email;
            $customers['mobile'] = $mobile;
            $customers['dob'] = $dob;
            $customers['address'] = $address;

            //load the model..
            $this->load->model('AdminDb');
            $result = $this->AdminDb->addCustomer($customers);
        } else if ($this->input->post("submit") == "UpdateMe") {


            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $mobile = $this->input->post('mobile');
            $dob = $this->input->post('dob');
            $address = $this->input->post('address');

            $customers = array(); //parameter which i pass in model
            $customers['name'] = $name;
            $customers['email'] = $email;
            $customers['mobile'] = $mobile;
            $customers['dob'] = $dob;
            $customers['address'] = $address;

            //load the model..
            $this->load->model('AdminDb');
            $result = $this->AdminDb->update_record($customers, (int) $this->input->post("id"));
        } else if ($this->input->post("submit") == "DeleteMe") {
            $this->load->model('AdminDb');
            $result = $this->AdminDb->delete_record((int) $this->input->post("id"));
        }
        //$this->load->view('admin/admin.php');
    }

    public function get_single_data() {
        $this->load->model('AdminDb');
        $id = $this->input->post("id");

        echo json_encode($this->AdminDb->get_single_data($id)); //converting string values to json format...
    }

    public function Admin_index() {
        //$this->load->view("layout/header.php");
        $this->load->view("admin/admin_index.php");
        //$this->load->view("layout/footer");
    }

    public function employees() {
        $this->load->model('AdminDb');
        $this->add_new_employee();
        $data['list'] = $this->AdminDb->getEmployee();
        $this->load->view('admin/employee', $data);
        //$this->load->view('layout/footer');
    }

    public function add_new_employee() {

        //button name and value from admin.php(views)
        if ($this->input->post("submit") == "Addemployee") {

            $emp_name = $this->input->post('emp_name');
            $dob = $this->input->post('dob');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');


            $employee = array(); //parameter which i pass in model
            $employee['emp_name'] = $emp_name;
            $employee['dob'] = $dob;
            $employee['email'] = $email;
            $employee['phone'] = $phone;


            //load the model..
            $this->load->model('AdminDb');
            $result = $this->AdminDb->adding_employee($employee);
        } else if ($this->input->post("submit") == "UpdateEmployee") {


            $emp_name = $this->input->post('emp_name');
            $dob = $this->input->post('dob');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');


            $employee = array(); //parameter which i pass in model
            $employee['emp_name'] = $emp_name;
            $employee['dob'] = $dob;
            $employee['email'] = $email;
            $custoemployeemers['phone'] = $phone;


            //load the model..
            $this->load->model('AdminDb');
            $result = $this->AdminDb->update_employee($employee, (int) $this->input->post("emp_id"));
        } else if ($this->input->post("submit") == "DeleteEmployee") {
            $this->load->model('AdminDb');
            $result = $this->AdminDb->delete_employee((int) $this->input->post("emp_id"));
        }
    }

    public function get_single_employee() {
        $this->load->model('AdminDb');
        $id = $this->input->post("emp_id");

        echo json_encode($this->AdminDb->get_single_employee($emp_id)); //converting string values to json format...
    }

}
