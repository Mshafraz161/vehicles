<?php
	class Contact extends CI_Controller{

		public function Contacts(){
				
                $this->load->model('AdminDb');
			    $this->add_new_contact();
                $data['contact']   =   $this->AdminDb->getContact();
				$this->load->view('admin/contact');
				$this->load->view('layout/footer');
		}

		public function AddNewContacts(){

				//button name and value from admin.php(views)
				if($this->input->post("submit") == "Addcontact"){
				
				$name = $this->input->post('name');
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');
				$msg = $this->input->post('message');
				
				
				$contact = array(); //parameter which i pass in model
				$contact['user_name']= $name;
				$contact['email']= $email;
				$contact['mobile']= $phone;
				$contact['message']= $msg;
				
				
				//load the model..
				$this->load->model('Contact_model');
				$result = $this->Contact_model->AddContact($contact);


		}
			$this->Load->view('contact/contact');
	}
}