<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events_controller extends CI_Controller {

	

	 public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('events');
	 	}


	public function index()
	{

		$data['event']=$this->events->get_all_events();
		$this->load->view('events_view',$data);
	}
	public function events_add()
		{
			$data = array(
					'message' => $this->input->post('message'),
					'place' => $this->input->post('place'),
					'day' => $this->input->post('day'),
					
				);
			$insert = $this->events->events_add($data);
			echo json_encode(array("status" => TRUE));
		}
		public function ajax_edit($id)
		{
			$data = $this->events->get_by_id($id);



			echo json_encode($data);
		}

		public function events_update()
	{
		$data = array(
				'message' => $this->input->post('message'),
				'place' => $this->input->post('place'),
				'day' => $this->input->post('day'),
				
			);
		$this->events->events_update(array('events_id' => $this->input->post('events_id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function events_delete($id)
	{
		$this->events->delete_events($id);
		echo json_encode(array("status" => TRUE));
	}


	


}
