<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Browse extends CI_Controller {

	

	 public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('browse_model');
	 	}


	public function index()
	{

		$data['books']=$this->browse_model->get_all_browse();
		$this->load->view('browse_view',$data);
	}
	public function browse_add()
		{
			$data = array(
					'name' => $this->input->post('name'),
					'vname' => $this->input->post('vname'),
					'mnumber' => $this->input->post('mnumber'),
					'cnumber' => $this->input->post('cnumber'),
					
				);
			$insert = $this->browse_model->browse_add($data);
			echo json_encode(array("status" => TRUE));
		}
		public function ajax_edit($id)
		{
			$data = $this->browse_model->get_by_id($id);



			echo json_encode($data);
		}

		public function browse_update()
	{
		$data = array(
				'name' => $this->input->post('name'),
				'vname' => $this->input->post('vname'),
				'mnumber' => $this->input->post('mnumber'),
				'cnumber' => $this->input->post('cnumber'),
				
			);
		$this->browse_model->browse_update(array('browse_id' => $this->input->post('browse_id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function browse_delete($id)
	{
		$this->browse_model->delete_browse($id);
		echo json_encode(array("status" => TRUE));
	}



}
