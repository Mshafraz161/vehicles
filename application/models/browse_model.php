<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Browse_model extends CI_Model
{

	var $table = 'browse';


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


		public function get_all_browse()
	{
		$this->db->from('browse');
		$query=$this->db->get();
		return $query->result();
	}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('browse_id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function browse_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function browse_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}
	

	public function delete_browse($id)
	{
		$this->db->where('browse_id', $id);
		$this->db->delete($this->table);
	}


}
