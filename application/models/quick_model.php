<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quick_model extends CI_Model
{

	var $table = 'quick';


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


public function get_all_quicks()
{
$this->db->from('quick');
$query=$this->db->get();
return $query->result();
}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('quick_id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function quick_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function quick_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where('quick_id', $id);
		$this->db->delete($this->table);
	}


}
