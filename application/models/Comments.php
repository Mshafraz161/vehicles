<?php 
	class Comments extends CI_Model{

		public function __construct(){

		}

		public function InsertComments($comments){
		$this->db->insert('comments', $comments);
        return;
		}

		public function get_comments(){
		$query = $this->db->get('comments');
		return $query->result();
	}

		//update record from database
	public function update_comments($comments,$id){
        $this->db->trans_start();
		$this->db->where('id', $id);
		$this->db->update('comments', $comments);
        //echo $this->db->last_query();
        $this->db->trans_complete();
	}

	public function delete_comments($id){
		$this->db->where('id', $id);
		$this->db->delete('comments');
	}
	}
?>