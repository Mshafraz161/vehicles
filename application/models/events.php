<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Model
{

	var $table = 'events';


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


		public function get_all_events()
	{
		$this->db->from('events');
		$query=$this->db->get();
		return $query->result();
	}


	public function get_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->where('events_id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function events_add($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function events_update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_events($id)
	{
		$this->db->where('events_id', $id);
		$this->db->delete($this->table);
	}


}
