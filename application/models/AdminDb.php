<?php

	class AdminDb extends CI_Model{

		public function __construct(){

		}
	//insert query..	
	public function addCustomer($customers){
		$this->db->insert('customers', $customers);
        return;
	}
	//selecting all the recored from database..
	public function get_record(){
		$query = $this->db->get('customers');
		return $query->result();
	}
	//update record from database
	public function update_record($customers,$id){
        $this->db->trans_start();
		$this->db->where('customer_id', $id);
		$this->db->update('customers', $customers);
        //echo $this->db->last_query();
        $this->db->trans_complete();
	}
	//delete record from database...
	public function delete_record($id){
		$this->db->where('customer_id', $id);
		$this->db->delete('customers');
	}
        
    public function getCustomers(){
        $res    =   $this->db->get('customers')->result();
        return $res;
    }

    public function get_single_data($id){
        $this->db->where('customer_id', $id);
        $res    =   $this->db->get('customers')->result();
        return $res;
    }






    //adding employee details...
    //indert employee
    public function adding_employee($employee){
    	$this->db->insert('employee', $employee);
    	return;
    }

    public function get_employee(){
    	$query = $this->db->get('employee');
    	return $query->result();
    }

    //update employee..
    public function update_employee($employee, $emp_id){
    	$this->db->trans_start();
    	$this->db->where('emp_id', $emp_id);
    	$this->db->update('employee', $employee);
    	$this->db->trans_complete();
    }

    //delete employees from table..
    public function delete_employee($emp_id){
    	$this->db->where('emp_id', $emp_id);
    	$this->db->delete('employee');
    }
    	public function getEmployee(){
        $res    =   $this->db->get('customers')->result();
        return $res;
    }

   	  public function get_employee_data($emp_id){
        $this->db->where('emp_id', $emp_id);
        $result    =   $this->db->get('employee')->result();
        return $result;
    } 
}



?>